var gulp = require('gulp'),
    runSequence = require('run-sequence'),
    sass = require('gulp-sass'),
    minify = require('gulp-minify'),
    concat = require('gulp-concat');

gulp.task('compress', function() {
    gulp.src('./src/vendor/js/*.js')
        .pipe(concat('libs.js'))
        .pipe(minify())
        .pipe(gulp.dest('./js'))
});

gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('watch', function () {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
});

gulp.task('default', function(cb) {
    runSequence('sass', 'compress', cb);
});

gulp.task('dev', function(cb) {
    runSequence('sass', 'compress', 'watch', cb);
});